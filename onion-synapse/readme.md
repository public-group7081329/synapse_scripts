## Set up June 2023

# Environment
- Debian 11
- Synape 1.86

# Scratch-pad steps.  

1) Install Debian and configure.

2) Install LEMP (Just nginx)
	- Can skip Maria
	- Can skip Certbot
	- Can skip PHP
	- modify /etc/nginx/nginx.conf
		- set server_names_hash_bucket_size 256;

3) Install TOR
	- sudo systemctl stop tor
	- Configure torrc

```
    HiddenServiceDir /var/lib/tor/matrix/
		HiddenServicePort 80 127.0.0.1:80
		HiddenServicePort 443 127.0.0.1:443
		HiddenServicePort 8448 127.0.0.1:8448
		SOCKSPort 127.0.0.1:9050
		AutomapHostsOnResolve 1
		DNSPort 127.0.0.2:53
```

	- Start and note hostname or perform vanity url (copy vanity details as SU)
	- exit SU (if used)
	- Can confirm TOR running (should get default NGINX page for now)
	- sudo tor (to observe output, look for errors)
	- navigate to /var/lib/tor and check permissions for hidden service. Owner and User should be (example:) debian-tor [112] debian-tor [106] for files and folders 
	- sudo systemctl start tor (check default nginx page still loads)	

4) Create matrix.con /etc/nginx/sites-available
	The example below will work (After updating your sername) There may be further tweaks an NGINX expert can suggest. 

```
  server {
        server_name youruglyonionhoogcklkehtavf2rnlaojodeqzid.onion;

        listen 80;
        listen [::]:80;

        listen 443 ssl http2 default_server;
        listen [::]:443 ssl http2 default_server;

        listen 8448 ssl http2 default_server;
        listen [::]:8448 ssl http2 default_server;
        include snippets/self-signed.conf;

        location ~* ^(\/_matrix|\/_synapse|\/_client) {
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;
        client_max_body_size 512M;
        }

        location /.well-known/matrix/client {
        return 200 '{"m.homeserver": {"base_url": "https://youruglyonionhoogcklkehtavf2rnlaojodeqzid.onion"}}';
        default_type application/json;
        add_header Access-Control-Allow-Origin *;
        }

        location /.well-known/matrix/server {
        return 200 '{"m.server": "youruglyonionhoogcklkehtavf2rnlaojodeqzid.onion:443"}';
        default_type application/json;
        add_header Access-Control-Allow-Origin *;
        }
	}
```

5) Install SelfSigned SSL (https://www.howtogeek.com/devops/how-to-create-and-use-self-signed-ssl-on-nginx/) (No need to do http redirect)
	- Common name: Desired TOR Host Name (Vanity) or assigned TOR domain

6) sudo ln -s /etc/nginx/sites-available/matrix.conf /etc/nginx/sites-enabled/matrix.conf

7) sudo rm /etc/nginx/sites-enabled/default

8) sudo nginx -t check for errors. sudo systemctl restart nginx

9) Install Privoxy
	- sudo cp /etc/privoxy/config /etc/privoxy/config.bak
	- sudo rm /etc/privoxy/config
	- sudo nano /etc/privoxy/config & paste following

```
		listen-address  127.0.0.1:8118
		forward-socks5t / 127.0.0.1:9050 .
```

  - Stop / Start privoxy

10) Install Synapse

```
sudo apt install -y lsb-release wget apt-transport-https
sudo wget -O /usr/share/keyrings/matrix-org-archive-keyring.gpg https://packages.matrix.org/debian/matrix-org-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/matrix-org-archive-keyring.gpg] https://packages.matrix.org/debian/ $(lsb_release -cs) main" |
sudo tee /etc/apt/sources.list.d/matrix-org.list
sudo apt update
sudo apt install matrix-synapse-py3
```
	
11) sudo nano /etc/systemd/system/multi-user.target.wants/matrix-synapse.service

- Add the following Lines

```
Environment="matrix_proxy=http://127.0.0.1:8118"
Environment="http_proxy=http://127.0.0.1:8118"
Environment="https_proxy=http://127.0.0.1:8118"
```

- sudo systemctl daemon-reload
- sudo systemctl restart matrix-synapse.service

12) Update homeserver.yaml

```
pid_file: "/var/run/matrix-synapse.pid"
listeners:
  - port: 8008
    tls: false
    type: http
    x_forwarded: true
    bind_addresses: ['::1', '127.0.0.1']
    resources:
      - names: [client, federation]
        compress: false
web_client_location: <http://webclient.onion>
#DB
database:
  name: psycopg2
  args:
    user: synapse_user
    password: <password>
    database: synapse
    host: localhost
    cp_min: 5
    cp_max: 10
#Log
log_config: "/etc/matrix-synapse/log.yaml"
#Media Config
media_store_path: /mnt/hdd/storage
max_upload_size: 512M
ui_auth:
    session_timeout: "300s"
#Signing
signing_key_path: "/etc/matrix-synapse/homeserver.signing.key"
#User Registration
enable_registration: true
enable_registration_without_verification: true
registration_shared_secret: "<StringValue>"
#KeyServers
trusted_key_servers:
  - server_name: "matrix.org"
  - server_name: "kawaiiloli.twilightparadox.com"
  - server_name: "matrix.fx47ny2u4eykxwe4p4atmdcozamahqszyiglszrfor2cchcw22wiimid.onion"
    accept_keys_insecurely: false
suppress_key_server_warning: true
#Federation
serve_server_wellknown: true
allow_public_rooms_over_federation: true
allow_public_rooms_without_auth: true
federation_certificate_verification_whitelist:
  - "*.onion"
federation_verify_certificates: true
federation:
  client_timeout: 10m
  max_short_retry_delay: 5m
  max_long_retry_delay: 10m
  max_short_retries: 5
  max_long_retries: 20
#IP LogginG#
user_ips_max_age: 1m
#Redaction Delete Message#
redaction_retention_period: 5m
#Delete Stale Devices#
delete_stale_devices_after: 7d
#Room Encryption
#encryption_enabled_by_default_for_room_type: all
#IpWhiteList - Added from this entry: https://github.com/matrix-org/synapse/issues/5152#issuecomment-1570540309 (Step 6)
ip_range_whitelist:
  - '127.0.0.0/8'
  - 'fe80::/10'
#User Directory
user_directory:
    enabled: true
    search_all_users: true
    prefer_local_users: true
#Forget Rooms
forget_rooms_on_leave: true
#Presence
presence:
  enabled: false
```

13) sudo systemctl restart matrix

	
12) create admin user.
	sudo register_new_matrix_user -u admin -p <urpassword> -a -c /etc/matrix-synapse/homeserver.yaml