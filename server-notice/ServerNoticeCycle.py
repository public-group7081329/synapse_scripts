import json
import time
import requests
 
# Replace these values with your homeserver URL, access token, and room ID
HOMESERVER_URL = "https://<ur server address>"
ACCESS_TOKEN = "syt_<ur token>"
message_body = "Ur Message"
 
headers = {
    "Authorization": f"Bearer {ACCESS_TOKEN}",
    "Content-Type": "application/json"
}
 
next_token = None
 
# Fetch all users on the matrix server
user_names = []
 
while True:
    if next_token is None:
        response = requests.get(f"{HOMESERVER_URL}/_synapse/admin/v2/users", headers=headers)
    else:
        response = requests.get(f"{HOMESERVER_URL}/_synapse/admin/v2/users?from={next_token}", headers=headers)
 
    if response.status_code == 200:
        users = json.loads(response.text)["users"]
        user_names.extend([user["name"] for user in users])
        print(user_names)
 
        if "next_token" in json.loads(response.text):
            next_token = json.loads(response.text)["next_token"]
        else:
            break
    else:
        print(f"Error: {response.status_code} - {response.text}")
        exit(1)
 
# Send a system notification to each user
for user_id in user_names:
    while True:
        response = requests.post(f"{HOMESERVER_URL}/_synapse/admin/v1/send_server_notice", headers=headers, json={
            "user_id": user_id,
            "content": {
                "msgtype": "m.text",
                "body": message_body,
            }
        })
        if response.status_code == 429:
            retry_after_ms = response.json().get("retry_after_ms", 0)
            print(f"Too Many Requests. Retrying after {retry_after_ms / 1000} seconds...")
            time.sleep(retry_after_ms / 1000)
        elif response.status_code == 200:
            break
        else:
            print(f"Failed to send system notification to {user_id}: {response.text}")
            break
 
print("System notifications sent successfully!")
 