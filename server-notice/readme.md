# Server Notice. Error handling for ratelimiting included.

## Synapse Documentation
https://matrix-org.github.io/synapse/latest/admin_api/server_notices.html

## Send system notices to all users on a server. 
Add Server Address, Token, Message.

## Required Work 
Add capacity to retrieve more than 100 users.

## Original Author
- Ghost